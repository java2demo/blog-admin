package cc.xulu;

import cc.xulu.controller.*;
import cc.xulu.model.*;
import com.jfinal.config.*;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.Restful;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class LublogConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		me.setDevMode(true);
	}

	@Override
	public void configRoute(Routes me) {
        me.add("/post", PostController.class);
        me.add("/category", CategoryController.class);
        me.add("/",IndexController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		loadPropertyFile("config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password"));
		me.add(c3p0Plugin);

		ActiveRecordPlugin activeRecord = new ActiveRecordPlugin(c3p0Plugin);
		me.add(activeRecord);
		activeRecord.addMapping("user", User.class);
		activeRecord.addMapping("post", Post.class);
		activeRecord.addMapping("user", User.class);
		activeRecord.addMapping("category", Category.class);
		activeRecord.addMapping("tag", Tag.class);
		activeRecord.addMapping("comment", Comment.class);

	}

	@Override
	public void configInterceptor(Interceptors me) {
		//注册全局监听器
//        me.add(new Restful());
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler());
	}

}
