package cc.xulu.controller;

import cc.xulu.common.Status;
import com.jfinal.core.Controller;

import cc.xulu.model.*;

public class TagController extends Controller {
	public void index() {
		setAttr("tagPage", Tag.dao.paginate(getParaToInt(0, 1), 10, "select *", "from tag order by id asc"));
		render("list.html");
	}
	
	public void add() {
	    render("edit.html");
	}

    public void edit() {
        setAttr("tag", Tag.dao.findById(getParaToInt()));
        render("edit.html");
    }
	
	public void save() {
		Tag tag = getModel(Tag.class);

		tag.save();

        renderJson(tag);
	}

    public void show() {
        Tag tag = Tag.dao.findById(getParaToInt());

        renderJson(tag);

    }
	
	public void update() {
        Tag tag = getModel(Tag.class);
        tag.update();
        renderJson(new Status());
	}
	
	public void delete() {
        Tag.dao.deleteById(getParaToInt());
        renderJson(new Status());
	}
}
