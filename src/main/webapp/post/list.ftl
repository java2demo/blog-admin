<#include "/common/_layout.ftl"/>
<@layout>

<div class="container">
    <!--the content-->
    <div class="ui grid">
        <!--the vertical menu-->
        <div class="four wide column">
            <div class="verticalMenu">
                <div class="ui vertical pointing menu fluid">
                    <a class="item" href="/post/add">
                        <i class="rocket icon"></i> 发布博文
                    </a>
                    <a class="active teal item" href="/post">
                        <i class="cloud icon"></i> 我的文章
                    </a>
                    <a class="item" href="#">
                        <i class="bar chart icon"></i> 文章统计
                    </a>
                </div>
            </div>
        </div>

        <div class="twelve wide column">
            <div class="pageHeader">
                <div class="segment">
                    <h3 class="ui dividing header">
                        <i class="large setting icon"></i>
                        <div class="content">
                            文章管理
                            <div class="sub header">管理所有已发布的文章</div>
                        </div>
                    </h3>
                </div>
            </div>

            <div class="ui vertical segment">
                <div class="ui small left icon input">
                    <input type="text" placeholder="输入关键字...">
                    <i class="search icon"></i>
                </div>
                <a class="circular ui mini active button" title="全部">全部</a>
                <a class="circular ui mini button" title="已发布">已发布</a>
                <a class="circular ui mini button" title="未发布">未发布</a>
                <a class="circular ui mini button" title="已冻结">已冻结</a>
            </div>

            <!-- the produc table-->
            <table class="ui compact table">
                <thead>
                <tr>
                    <th>编 号</th>
                    <th>主 题</th>
                    <th>发布时间</th>
                    <th>状 态</th>
                    <th>操 作</th>
                </tr>
                </thead>
                <tbody>
                <#list postPage.getList() as p>
                <tr>
                    <td>${p.id}</td>
                    <td>${p.title}</td>
                    <td>${p.post_date}</td>
                    <td><i class="icon checkmark"></i></td>
                    <td>
							<span class="mini ui buttons">
								<a class="ui button disabled">发 布</a>
								<a class="ui green button">冻 结</a>
								<a class="ui basic button">删 除</a>
							</span>
                    </td>
                </tr>
                </#list>

                </tbody>
            </table>

            <#include "/common/_paginate.ftl" />
            <@paginate currentPage=postPage.pageNumber totalPage=postPage.totalPage actionUrl="${CONTEXT_PATH}/post/" />
        </div>
    </div>
</div>

</@layout>

<@scripts>

</@scripts>