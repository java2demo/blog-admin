<#include "/common/_layout.ftl"/>
<@layout>

<div class="container">
    <!--the content-->
    <div class="ui grid">
        <!--the vertical menu-->
        <div class="four wide column">
            <div class="verticalMenu">
                <div class="ui vertical pointing menu fluid">
                    <a class="active teal item" href="/post/add">
                        <i class="rocket icon"></i> 发布博文
                    </a>
                    <a class="item" href="/post">
                        <i class="cloud icon"></i> 我的文章
                    </a>
                    <a class="item" href="#">
                        <i class="bar chart icon"></i> 文章统计
                    </a>
                </div>
            </div>
        </div>

        <div class="twelve wide column">
            <div class="pageHeader">
                <div class="segment">
                    <h3 class="ui dividing header">
                        <i class="large add icon"></i>
                        <div class="content">
                            添加文章
                        </div>
                    </h3>
                </div>
            </div>
            <div class="ui form fluid vertical segment">
                <form name="form" action="/post/save" method="post">
                    <div class="two fields">
                        <div class="field">
                            <label>标题</label>
                            <div class="ui small left labeled icon input">
                                <input type="text" placeholder="请输入文章标题" id="device_title" name="post.title" value=""/>
                                <i class="exchange icon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label>类别</label>
                        <div class="ui dropdown selection">
                            <input type="hidden" name="post.category_id" value="0">
                            <div class="default text">原创</div>
                            <i class="dropdown icon"></i>
                            <div class="menu" name="sensor_type" id="sensor_type">
                                <div class="item active" data-value="0" value="0">原创</div>
                                <div class="item" data-value="1" value="1">转载</div>
                                <div class="item" data-value="2" value="2">翻译</div>

                            </div>
                        </div>
                    </div>
                    <div class="two fields">
                        <div class="field">
                            <div class="field">
                                <label>标签</label>
                                <input type="text" id="sensor_tags" name="sensor_tags"/>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label>内容</label>
                        <textarea name="post.content" placeholder=""></textarea>
                    </div>
                    <input class="ui small blue submit button" type="submit" value="保存">
                    <input class="ui small basic button" type="reset" value="取消">
                </form>
                <!--the form end-->
            </div>
        </div>
    </div>
</div>

</@layout>

<@scripts>
<link type="text/css" rel="stylesheet" href="${CONTEXT_PATH}/resource/css/jquery.tagsinput.css" />
<script type="text/javascript" src="${CONTEXT_PATH}/resource/javascript/jquery.tagsinput.js"></script>
<script>
    $('#sensor_tags').tagsInput();
</script>
</@scripts>